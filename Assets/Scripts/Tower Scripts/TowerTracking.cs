﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerTracking : MonoBehaviour
{
	public GameObject player;
	public float speed = 5;
	public GameObject target;
	GameObject first = null;
	GameObject last = null;
	GameObject lowest = null;
	GameObject highest = null;
	public GameObject EndZone;
	Vector3 lastKnownPosition = Vector3.zero;
	Quaternion lookAtRotation;
	public int trackRadius;
	SphereCollider sc;
	public List<GameObject> trackingList = new List<GameObject>();
	float distance;
	float health;
	public int trackCount;
	bool canInteract = false;
	public float trackPriority = 1;
	RaycastHit hit;
	bool isVisible;

	// Use this for initialization
	void Start()
	{
		sc = GetComponent<SphereCollider>();
		sc.radius = trackRadius;
		distance = Mathf.Infinity;
		health = Mathf.Infinity;
		EndZone = GameObject.FindWithTag("Base");
	}

	// Update is called once per frame
	void Update()
	{
        EndZone = GameObject.FindWithTag("Base");
        //target = GameObject.FindWithTag("Enemy");
        trackCount = trackingList.Count;
		if (trackCount > 0)
		{
			if (trackPriority == 1)
			{
				target = TrackNearest();
			}
			else if (trackPriority == 2)
			{
				target = TrackFurthest();
			}
			else if (trackPriority == 3)
			{
				target = TrackLowest();
			}
			else if (trackPriority == 4)
			{
				target = TrackHighest();
			}
			//target = GameObject.FindWithTag("Enemy");
		}
		else
		{
			target = null;
		}



		if (target != null && CheckVisibiity(target) == true)
		{

			//target = FindFirst();
			//if ((Vector3.Distance(transform.position, target.transform.position) < maxRange)
			//&& (Vector3.Distance(transform.position, target.transform.position) > minRange))

			if (lastKnownPosition != target.transform.position)
			{
				lastKnownPosition = target.transform.position;
				lookAtRotation = Quaternion.LookRotation(Vector3.Scale(lastKnownPosition, new Vector3(1, 0, 1)) - transform.position);
			}
			if (transform.FindChild("Pivot").rotation != lookAtRotation)
				//if (transform.rotation != lookAtRotation)
			{
				//transform.rotation = Quaternion.RotateTowards(transform.rotation, lookAtRotation, speed * Time.deltaTime);

				transform.FindChild("Pivot").rotation = Quaternion.RotateTowards(transform.FindChild("Pivot").rotation, lookAtRotation, speed * Time.deltaTime);
			}
			transform.FindChild("Pivot").FindChild("barrel").Rotate(0, 0, 30f);

		}
		else
		{
			target = null;
		}

		if (canInteract)
		{
			if (Input.GetKeyDown(KeyCode.C))
			{
				if (player != null)
				{

				}
			}
		}

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy")
		{
			trackingList.Add(other.gameObject);


		}

		if (other.tag == "Player")
		{
			player = other.gameObject;
			canInteract = true;
		}

	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Enemy")
		{
			trackingList.Remove(other.gameObject);
		}

		if (other.tag == "Player")
        {
            canInteract = false;
        }
	}


	public GameObject TrackNearest()
	{
		distance = Mathf.Infinity;
		foreach (GameObject enemy in trackingList)
		{
			if (enemy)
			{
				Vector3 diff = enemy.transform.position - EndZone.transform.position;
				float curDistance = diff.sqrMagnitude;
				if ((curDistance < distance) && CheckVisibiity(enemy) == true)
				{
					first = enemy;
					distance = curDistance;
					//target = go;
				}
			}
			else
			{
				trackingList.Remove(enemy);
			}
		}
		return first;
	}

	public GameObject TrackFurthest()
	{
		distance = 0;
		foreach (GameObject enemy in trackingList)
		{
			if (enemy)
			{
				Vector3 diff = enemy.transform.position - EndZone.transform.position;
				float curDistance = diff.sqrMagnitude;
				if ((curDistance > distance) && CheckVisibiity(enemy) == true)
				{
					last = enemy;
					distance = curDistance;
					//target = go;
				}
			}
			else
			{
				trackingList.Remove(enemy);
			}
		}
		return last;
	}

	public GameObject TrackLowest()
	{
		health = Mathf.Infinity;
		foreach (GameObject enemy in trackingList)
		{
			if (enemy)
			{
				Enemy curEnemy = enemy.GetComponent<Enemy>();
				float curHealth = curEnemy.currentHealth;
				if ((curHealth < health) && CheckVisibiity(enemy) == true)
				{
					lowest = enemy;
					health = curHealth;
					//target = go;
				}
			}
			else
			{
				trackingList.Remove(enemy);
			}
		}
		return lowest;
	}

	public GameObject TrackHighest()
	{
		health = 0;
		foreach (GameObject enemy in trackingList)
		{
			if (enemy)
			{
				Enemy curEnemy = enemy.GetComponent<Enemy>();
				float curHealth = curEnemy.currentHealth;
				if ((curHealth > health) && CheckVisibiity(enemy) == true)
				{
					highest = enemy;
					health = curHealth;
					//target = go;
				}
			}
			else
			{
				trackingList.Remove(enemy);
			}
		}
		return highest;
	}

	public void SwitchTracking()
	{
		trackPriority++;
		if (trackPriority >= 5)
		{
			trackPriority = 1;
		}
	}

	public bool CheckVisibiity(GameObject enemyTarget)
	{
		if (Physics.Raycast(transform.position, (enemyTarget.transform.position - transform.position), out hit, trackRadius))
		{
			if (hit.collider.gameObject.CompareTag("Enemy"))
			{
				isVisible = true;
				//print("lul");
			}
			else
			{
				isVisible = false;
				//print("lmao");
			}
		}

		return isVisible;
	}
}