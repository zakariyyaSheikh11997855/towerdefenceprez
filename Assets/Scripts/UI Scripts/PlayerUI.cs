﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Text))]
public class PlayerUI : MonoBehaviour {
	[SerializeField]
	RectTransform playerHealthBarFill;
	[SerializeField]
	RectTransform baseHealthBarFill;
	[SerializeField]
	Text txtFunds;
	[SerializeField]
	Text txtAmmo;
	[SerializeField]
	Text txtPhase;
	[SerializeField]
	Text txtGameOver;
	[SerializeField]
	Text txtTowerType;
	[SerializeField]
	Text txtTowerPrice;

	bool showShop;

	public GameObject shopUI;
	public GameObject ammoUI;
	public GameObject gameOverUI;

	Base b;
	GridInteraction gi;
	private GameObject player;
	public void SetPlayer(GameObject value) {
		gi = value.GetComponent<GridInteraction> ();
		player = value; 

		showShop = false;
	}

	void Awake(){
		b = GameObject.FindGameObjectWithTag ("Base").GetComponent<Base>();
	}
	void Update(){
		if (GameManager.state != GameManager.GameState.LOST) {
			SetBaseHealthBar (b.currentHealth / b.maxHealth);
			SetPhaseTxt (GameManager.state, (int)GameManager.nextPhaseCountdown);
			SetPlayerFundsTxt (player.GetComponent<Player> ().funds);
			if (gi != null) {
				Tower tower = gi.currentTower;
				if (tower != null) {
					SetTowerTxt (tower.price, tower.type);
				}
			}

			//Show the Shop UI.
			if (GameManager.state == GameManager.GameState.PHASE_BUILDING) {
				if (Input.GetKeyDown (KeyCode.Tab)) {
					showShop = !showShop;
				}
			}

			//Turn off shop if fighting
			if (GameManager.state == GameManager.GameState.PHASE_FIGHTING) {
				showShop = false;
			}

			if (showShop) {
				shopUI.SetActive (true);
				player.GetComponent<RigidbodyFirstPersonController> ().mouseLook.SetCursorLock (false);
			} else {
				shopUI.SetActive (false);
				player.GetComponent<RigidbodyFirstPersonController> ().mouseLook.SetCursorLock (true);
			}

			if (((WeaponManger)FindObjectOfType (typeof(WeaponManger))).isActive) {
				ammoUI.SetActive (true);
			} else {
				ammoUI.SetActive (false);
			}
		} else {
			if (shopUI.activeInHierarchy) {
				shopUI.SetActive (false);
			}

			gameOverUI.SetActive (true);
			player.GetComponent<RigidbodyFirstPersonController> ().mouseLook.SetCursorLock (false);
			GameObject ch = GameObject.FindGameObjectWithTag ("Crosshair");
			if (ch.activeInHierarchy) {
				ch.SetActive (false);
			}

		}

	}
	private void SetPlayerFundsTxt(int _funds){
		txtFunds.text = _funds.ToString();
	}
	public void SetPlayerHealthBar(float _amount){
		playerHealthBarFill.localScale = new Vector3 (_amount, 1f, 1f);
		Debug.Log ("bar " + _amount);
	}
	private void SetBaseHealthBar(float _amount){

        baseHealthBarFill.localScale = new Vector3 (_amount, 1f, 1f);
	}
	private void SetPhaseTxt(GameManager.GameState  _state, int _countdown){
		if (_state == GameManager.GameState.PHASE_BUILDING) {
			txtPhase.text = "Building: " + _countdown + "s";
		} else if (_state == GameManager.GameState.PHASE_FIGHTING) {
			txtPhase.text = "Battle!";
		}
	}
	private void SetTowerTxt(int _price, string _type){
		txtTowerPrice.text = "- $" + _price;
		txtTowerType.text = _type;
	}
}
