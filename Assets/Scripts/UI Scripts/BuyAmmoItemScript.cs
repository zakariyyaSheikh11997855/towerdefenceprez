﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyAmmoItemScript : BuyButton {

	public GameObject weaponToBuy;
	public int amountOfAmmo;
	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		labelText.GetComponent<Text> ().text = "+ " + amountOfAmmo;
	}

	public void OnClick(){
		WeaponManger weaponManager = player.GetComponent<WeaponManger>();//GameObject.FindGameObjectWithTag ("Player").GetComponent<WeaponManger>();
		weaponManager.GetComponent<WeaponManger>().ChangeToWeapon (weaponToBuy.GetComponent<BaseWeapon>());
		if (weaponManager.addAmmo (amountOfAmmo)) {
			base.OnMouseDown ();
		}
	}
}
